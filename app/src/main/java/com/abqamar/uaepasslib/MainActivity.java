package com.abqamar.uaepasslib;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.abqamar.uaepasslib.uaepass.UaePassAuthorizationModel;
import com.abqamar.uaepasslib.uaepass.UaePassCallback;
import com.abqamar.uaepasslib.uaepass.UaePassUserModel;
import com.abqamar.uaepasslib.uaepass.UaePassUtil;
import com.google.gson.Gson;

import static com.abqamar.uaepasslib.uaepass.UaePassUtil.REDIRECT_URL;
import static com.abqamar.uaepasslib.uaepass.UaePassUtil.UAEPASS_AUTH_CALLBACK;
import static com.abqamar.uaepasslib.uaepass.UaePassUtil.UAEPASS_PROFILE_CALLBACK;

public class MainActivity extends AppCompatActivity implements UaePassCallback {

    public String lang = "en";
    private String state = "";

    private String mSuccessURLUAEPass = "";
    private String mFailureURLUAEPass = "";
    private WebView webView;

    private AlertDialog webViewAlertDialog;

    ProgressDialog progress;
    
    private ImageView smartPassBtn;
    private TextView welcomeName;

    private void createProgressDialog(){
        progress = new ProgressDialog(this);
        progress.setTitle(null);
        progress.setMessage(getString(R.string.banner_loading));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void showProgress(){
        if(progress != null && !progress.isShowing()){
            progress.show();
        }
    }

    private void hideProgress(){
        if(progress != null && progress.isShowing()){
            progress.cancel();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.e("data", "i am here in onNewIntent");
        if (intent.getDataString().equals(REDIRECT_URL)) {
            webView.loadUrl(mSuccessURLUAEPass);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        welcomeName = findViewById(R.id.welcomeName);
        smartPassBtn = findViewById(R.id.smartPassBtn);

        createProgressDialog();

        smartPassBtn.setOnClickListener(v -> {
            state = "";
            state = UaePassUtil.generateRandomString(24);

            String url;
            if (UaePassUtil.appInstalledOrNot(this, UaePassUtil.UAE_PASS_PACKAGE_ID)) {
                url = UaePassUtil.UAEPASS_AUTHENTICATION_URL + "?redirect_uri=" + REDIRECT_URL + "&client_id=" + UaePassUtil.UAE_PASS_CLIENT_ID + "&state=" + state + "&response_type=" + UaePassUtil.RESPONSE_TYPE + "&scope=" + UaePassUtil.SCOPE + "&acr_values=" + UaePassUtil.ACR_VALUES_MOBILE + "&ui_locales=" + lang;
            } else {
                url = UaePassUtil.UAEPASS_AUTHENTICATION_URL + "?redirect_uri=" + REDIRECT_URL + "&client_id=" + UaePassUtil.UAE_PASS_CLIENT_ID + "&state=" + state + "&response_type=" + UaePassUtil.RESPONSE_TYPE + "&scope=" + UaePassUtil.SCOPE + "&acr_values=" + UaePassUtil.ACR_VALUES_WEB + "&ui_locales=" + lang;
            }
            login(url);
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void login(String url) {

        
        CookieManager.getInstance().removeAllCookie();

        webViewAlertDialog = new AlertDialog.Builder(this).create();
        webViewAlertDialog.setTitle(null);

        RelativeLayout dialogView = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.uaepass_login, null);
        webViewAlertDialog.setView(dialogView);
        webViewAlertDialog.setCancelable(true);
        webView = dialogView.findViewById(R.id.webView);
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(false);
        webView.clearCache(true);
        webView.clearHistory();
        if (Build.VERSION.SDK_INT >= 26) {
            settings.setSafeBrowsingEnabled(false);
        }

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                if (url.contains("uaepass://digitalid")) {

                    if(!UaePassUtil.IS_PRODUCTION){
                        url = url.replace("uaepass://" , "uaepassqa://");
                    }

                    mSuccessURLUAEPass = UaePassUtil.getQueryParameterValue(url,"successurl");
                    mFailureURLUAEPass = UaePassUtil.getQueryParameterValue(url,"failureurl");

                    // success url
                    if(url.contains("successurl")){
                        url = UaePassUtil.replaceUriParameter(Uri.parse(url), "successurl", REDIRECT_URL).toString();
                    }

                    // failure url
                    if(url.contains("failureurl")){
                        url = UaePassUtil.replaceUriParameter(Uri.parse(url), "failureurl", REDIRECT_URL).toString();
                    }

                    /*if(url.contains("browserpackage")){

                        url = UaePassUtil.removeUriParameter(Uri.parse(url), "browserpackage").toString();

                        url = url + "&closeondone=true";
                    }*/

                    Intent launchIntent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    //launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PackageManager packageManager = getPackageManager();
                    if (launchIntent.resolveActivity(packageManager) != null) {
                        startActivity(launchIntent);
                    } else {
                        UaePassUtil.openUAEPassAppInPlayStore(MainActivity.this);
                        webViewAlertDialog.dismiss();
                    }

                    return true;
                } else {
                    String redirectUrl = REDIRECT_URL;
                    if (url.startsWith(redirectUrl)) {
                        String code = UaePassUtil.getQueryParameterValue(url, "code");
                        String state_ = UaePassUtil.getQueryParameterValue(url, "state");
                        String error = UaePassUtil.getQueryParameterValue(url, "error");

                        if (error != null) {
                            webViewAlertDialog.dismiss();
                            if(error.contains("access_denied")){
                                UaePassUtil.showErrorDialog(MainActivity.this, getString(R.string.user_cancelled_authentication), getString(R.string.error));
                            }else{
                                UaePassUtil.showErrorDialog(MainActivity.this, getString(R.string.smartpass_claims_error_msg), getString(R.string.error));
                            }
                            return false;
                        }

                        if (!state.equals(state_)) {
                            code = null;
                        }
                        if (code != null) {
                            callAccessTokenUaePass(code);
                        }
                        webViewAlertDialog.dismiss();
                        return true;
                    } else {
                        return false;
                    }
                }
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);


            }
        });
        EditText edit = (EditText) dialogView.findViewById(R.id.editText);
        edit.setFocusable(true);
        edit.requestFocus();
        webView.loadUrl(url);


        webViewAlertDialog.show();
    }

    private void callAccessTokenUaePass(String code) {
        showProgress();
        UaePassUtil.callUAEPassAccessToken(this, code, this);
    }

    private void callUaePassProfile(String accessToken) {
        showProgress();
        UaePassUtil.callUserProfile(this, accessToken, this);
    }

    @Override
    public void uaePassResponse(int type, String response) {
        hideProgress();
        if (type == UAEPASS_AUTH_CALLBACK) {
            UaePassAuthorizationModel response1 = new Gson().fromJson(response, UaePassAuthorizationModel.class);
            if (response1 != null && response1.getAccess_token() != null) {
                callUaePassProfile(response1.getAccess_token());
            } else {
                uaePassError(UAEPASS_AUTH_CALLBACK);
            }
        }

        if (type == UAEPASS_PROFILE_CALLBACK) {
            UaePassUserModel userInfo = new Gson().fromJson(response, UaePassUserModel.class);
            Log.e("user info", response);

            if (userInfo.getUserType().equalsIgnoreCase("SOP1")) {
                openSOP1ErrorDialog();
                return;
            }

            if (userInfo != null) {
                //callServiceFromUAEPass(userInfo);
                welcomeName.setText("Welcome, " + userInfo.getFullnameEN()+"");
            } else {
                uaePassError(UAEPASS_PROFILE_CALLBACK);
            }
        }
    }

    @Override
    public void uaePassError(int type) {
        hideProgress();
        UaePassUtil.showErrorDialog(this, getString(R.string.smartpass_claims_error_msg), getString(R.string.error));
    }

    private void openSOP1ErrorDialog() {
        final Dialog errorDialog = new Dialog(this);
        errorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        errorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        errorDialog.setCancelable(true);
        errorDialog.setContentView(this.getLayoutInflater().inflate(R.layout.dialog_sop1, null));

        TextView tvTitle = errorDialog.findViewById(R.id.sop_titlename);
        TextView tvMessage = errorDialog.findViewById(R.id.sop_message);
        Button btnSOPOk = errorDialog.findViewById(R.id.sop_ok_btn);
        ImageView sop1Close = errorDialog.findViewById(R.id.sop1Close);

        tvTitle.setText(getString(R.string.sop1_title));
        tvMessage.setMovementMethod(LinkMovementMethod.getInstance());
        tvMessage.setText(customTextView(), TextView.BufferType.SPANNABLE);

        sop1Close.setOnClickListener(v -> errorDialog.cancel());
        btnSOPOk.setOnClickListener(v -> {
            errorDialog.cancel();
            login(UaePassUtil.UAE_PASS_LOGOUT_URL);
        });

        errorDialog.show();

    }

    private SpannableStringBuilder customTextView() {
        if (lang.equalsIgnoreCase("en")) {
            SpannableStringBuilder spanTxt = new SpannableStringBuilder("To access the ABCApp Login you need to have a verified UAE PASS account. Please visit the nearest ");
            spanTxt.append("Kiosk");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://selfcare.uaepass.ae/locations")));
                }
            }, spanTxt.length() - "Kiosk".length(), spanTxt.length(), 0);
            spanTxt.append(" to upgrade your UAE PASS account.\n\nFor more information, please check ");
            //spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
            spanTxt.append("FAQ");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://selfcare.uaepass.ae/faq")));
                }
            }, spanTxt.length() - "FAQ".length(), spanTxt.length(), 0);
            spanTxt.append(" or contact center ");
            spanTxt.append("009714xxxxxxx");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:009714xxxxxxx")));
                }
            }, spanTxt.length() - "009714xxxxxxx".length(), spanTxt.length(), 0);

            return spanTxt;
        } else {
            SpannableStringBuilder spanTxt = new SpannableStringBuilder("للوصول الى ABCApp تسجيل الدخول يجب ان يكون حسابك معرفا لدى الهوية الرقمية. يرجى زيارة أقرب ");
            spanTxt.append("منفذ خدمة");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://selfcare.uaepass.ae/locations")));
                }
            }, spanTxt.length() - "منفذ خدمة".length(), spanTxt.length(), 0);
            spanTxt.append(" لترقية حسابك في الهوية الرقمية.\n\nللمزيد من المعلومات، يرجى الاطلاع على ");
            //spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
            spanTxt.append("الأسئلة المتكررة");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://selfcare.uaepass.ae/faq")));
                }
            }, spanTxt.length() - "الأسئلة المتكررة".length(), spanTxt.length(), 0);
            spanTxt.append("  او الاتصال على مركز الاتصال ");
            spanTxt.append("009714xxxxxxx");
            spanTxt.setSpan(new ClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:009714xxxxxxx")));
                }
            }, spanTxt.length() - "009714xxxxxxx".length(), spanTxt.length(), 0);

            return spanTxt;
        }
    }
}
