package com.abqamar.uaepasslib.uaepass;

public interface UaePassCallback {
    void uaePassResponse(int type, String response);
    void uaePassError(int type);
}
